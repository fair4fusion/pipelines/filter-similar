FROM ubuntu:18.04
RUN apt-get -qq update
RUN apt-get -qq -y install curl
ADD ./filter_similar.sh /usr/src/filter_similar.sh
RUN chmod +x /usr/src/filter_similar.sh
CMD ["/usr/src/filter_similar.sh"]
