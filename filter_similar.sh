#!/bin/bash

cd /shared/${experimentId}/inter_out/

all_files=$(find -type f -name '*.distance' | cut -d '_' -f1 | cut -d '/' -f2 | sort -u)

for file in $all_files
do
	mkdir -p $file/
	mv ${file}* $file/
done

echo "Done with creating directories"

for d in */ ; do
	cd $d
 	files=$(ls -1 *.distance)
	for file in $files
	do
		x=$(ls $file | cut -d '_' -f2 | cut -d '.' -f1)
		mv $file ${x}.distance
	done
	cd ..
done



for d in */ ; do
  curl "http://similarity.fair4fusion.iit.demokritos.gr/run" -X POST -F id=$d -F number_of_sim=5 -F experimentId=${experimentId}
  echo $p
done
